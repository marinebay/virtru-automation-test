describe('Virtu test for interview', function() {
  
  beforeEach(function () {
    browser.ignoreSynchronization = true;
  });

  afterEach(function () {
    browser.ignoreSynchronization = true;
  });

  var EC = protractor.ExpectedConditions;
  var timeout = 10000;

  it('log me into gmail', function() {

    browser.get('https://google.com/mail');

    browser.sleep(10000);

    browser.wait(EC.visibilityOf($('#identifierId')), timeout);
    
    element(by.css('#identifierId')).click();

    element(by.css('#identifierId')).sendKeys('geigermark3@gmail.com');
    
    browser.wait(EC.elementToBeClickable($('#identifierNext')), timeout);

    element(by.css('#identifierNext > content > span')).click();

    browser.wait(EC.visibilityOf($('input[type=password][name=password]')), timeout);
    
    element(by.css('input[type=password][name=password]')).sendKeys('iwgtRjFM93$!');
    
    browser.wait(EC.elementToBeClickable($('#passwordNext > content > span')), timeout);

    browser.sleep(3000);
    
    element(by.css('#passwordNext > content > span')).click();

    browser.wait(EC.visibilityOf($('#gbqfq')), timeout);

    browser.wait(EC.visibilityOf($$('.UI div table tbody tr').get(0)), timeout);

    $$(".UI div table tbody tr").get(0).click();

    browser.sleep(3000);

    browser.wait(EC.visibilityOf($('img[src*="blue_lock"]')), timeout);

    $$('img[src*="blue_lock"]').get(0).click();

    browser.sleep(3000);

    browser.getAllWindowHandles().then(function (handles) {
      browser.switchTo().window(handles[1]);

      browser.wait(EC.visibilityOf($('#content')), timeout);

      $$('div[data-email="geigermark3@gmail.com"]').click();

      browser.wait(EC.visibilityOf($('a.sendEmailButton')), timeout);

      $$('a.sendEmailButton').click();

      browser.sleep(3000);

      browser.driver.close();

      browser.switchTo().window(handles[0]);

      browser.sleep(3000);

      browser.wait(EC.visibilityOf($$('div.aio span a').get(0)), timeout);

      $$('div.aio span a').get(0).click();

      browser.sleep(3000);

      element(by.css('#gbqfq')).sendKeys('verify@virtru.com ');

      $('#gbqfb').click();

      browser.sleep(3000);

      var isVisibleRow = element.all(by.css('div.UI table tbody tr')).filter(function (elm) {
          return elm.isDisplayed().then(function (isDisplayed) {
          return isDisplayed;
        });
      }).first();

      isVisibleRow.click();

      browser.sleep(3000);

      $$('a[href*=email-activation]').get(0).click()

      browser.sleep(5000);

      browser.getAllWindowHandles().then(function (handles) {
        browser.switchTo().window(handles[1]);

        browser.sleep(5000);

        browser.wait(EC.visibilityOf($('html')), timeout);

        browser.sleep(5000);

        console.log($('html').getText());

        expect($('html').getText()).toContain('Mark Geiger Virtu automation test');
      });

    });

  });

});
