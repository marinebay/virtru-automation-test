package com.gscript.virtuscore;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static junit.framework.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GscriptGmailTest {

    public static void pause(Integer milliseconds) {
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            System.out.print(e.toString());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // Create a new instance of the Chrome driver
        // Notice that the remainder of the code relies on the interface, 
        // not the implementation.
        WebDriver driver = new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, 20);

        // And now use this to visit Google
        driver.get("https://google.com/mail");
        // Alternatively the same thing can be done like this
        // driver.navigate().to("http://www.google.com");

        driver.findElement(By.id("identifierId")).sendKeys("geigermark3@gmail.com");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("identifierNext")));

        driver.findElement(By.cssSelector("#identifierNext > content > span")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[type=password][name=password]")));

        driver.findElement(By.cssSelector("input[type=password][name=password]")).sendKeys("iwgtRjFM93$!");

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#passwordNext > content > span")));

        driver.findElement(By.cssSelector("#passwordNext > content > span")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".UI div table tbody tr")));

        driver.findElement(By.cssSelector(".UI div table tbody tr")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("img[src*=\"blue_lock\"]")));

        driver.findElement(By.cssSelector("img[src*=\"blue_lock\"]")).click();

        pause(3000);

        String originalTab = driver.getWindowHandle();

        ArrayList<String> newTab = new ArrayList<>(driver.getWindowHandles());

        newTab.remove(originalTab);

        // change focus to new tab
        driver.switchTo().window(newTab.get(0));

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[data-email=\"geigermark3@gmail.com\"]")));

        driver.findElement(By.cssSelector("div[data-email=\"geigermark3@gmail.com\"]")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a.sendEmailButton")));

        driver.findElement(By.cssSelector("a.sendEmailButton")).click();
        
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".send-another-email")));
        
        String pageContent = driver.findElement(By.cssSelector("html")).getText();

        System.out.print(pageContent);

        assertTrue(pageContent.contains("Check your inbox for\n" +
            "the verification email"));      
        
        driver.close();

        driver.switchTo().window(originalTab);

        pause(3000);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#gbqfq")));

        driver.findElement(By.cssSelector("#gbqfq")).sendKeys("verify@virtru.com");

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.aio span a")));

        driver.findElement(By.cssSelector("div.aio span a")).click();

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[email=\"verify@virtru.com\"]")));

        pause(3000);

        driver.findElement(By.cssSelector("span[email=\"verify@virtru.com\"]")).click();

        pause(3000);

        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href*=email-activation]")));

        driver.findElement(By.cssSelector("a[href*=email-activation]")).click();

        pause(3000);

        ArrayList<String> newTab2 = new ArrayList<>(driver.getWindowHandles());
        
        newTab2.remove(originalTab);

        // change focus to new tab
        driver.switchTo().window(newTab2.get(0));

        pause(3000);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("html")));

        pageContent = driver.findElement(By.cssSelector("html")).getText();

        System.out.print(pageContent);

        assertTrue(pageContent.contains("Mark Geiger Virtu automation test"));

        driver.quit();
    }
}