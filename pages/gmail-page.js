var gmail_page = function() {
     
    this.usernameTextBox = element(by.css('#username'));
    this.passwordTextBox = element(by.css('#password'));
    this.loginButton = element(by.css('input[value="Member Gmail"]'));
     
    this.enterUsername = function(name) {
        this.usernameTextBox.sendKeys(name);
    };
     
    this.enterPassword= function(password) {
        this.usernameTextBox.sendKeys(password);
    };
     
    this.clickGmail = function() {
        this.loginButton.click();
    };
};

var GmailPage = (function () {
    function GmailPage() {
        this.usernameTextBox = element(by.css('#username'));
        this.passwordTextBox = element(by.css('#password'));
        this.loginButton = element(by.css('input[value="Member Gmail"]'));
    }

    GmailPage.prototype.enterUsername = function(name) {
        this.usernameTextBox.sendKeys(name);
    };
     
    GmailPage.prototype.enterPassword = function(password) {
        this.passwordTextBox.sendKeys(password);
    };
     
    GmailPage.prototype.clickGmail = function() {
        this.loginButton.click();
    };

    GmailPage.prototype.visitPage = function () {
        browser.get("/");
    };

    GmailPage.prototype.fillEmail = function (email) {
        this.emailField.sendKeys(email);
    };

    GmailPage.prototype.fillPassword = function (password) {
        if (password == null) {
            password = "password";
        }
        this.passwordField.sendKeys(password);
    };

    GmailPage.prototype.login = function () {
        this.loginButton.click();
    };

    GmailPage.prototype.getCurrentUser = function () {
        return this.currentUser.getText();
    };

    return GmailPage;

})();

module.exports = GmailPage;
