var DealerPage = (function () {
    function DealerPage() {
        this.manufacturerTextBox = element(by.css('#manufacturer'));
        this.searchButton = element(by.css('input[value="Search Boats"]'));
    }

    DealerPage.prototype.enterManufacturer = function(manufacturer) {
        this.manufacturerTextBox.sendKeys(manufacturer);
    };
     
     
    DealerPage.prototype.submitForm = function() {
        this.searchButton.click();
    };

    DealerPage.prototype.visitDealerPage = function () {
        browser.get("http://dealer.marinebay.com");
    };


    return DealerPage;

})();

module.exports = DealerPage;
